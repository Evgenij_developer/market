package ua.luxoft.market.stories;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ua.luxoft.market.Configuration;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = Configuration.class)
public class LaptopStepsTest {

    @Autowired
    private LaptopChoose laptopChoose;

    @Test
    public void runLaptopChooseStory() throws Throwable {
        laptopChoose.run();
    }
}
