package ua.luxoft.market.dao.service;

import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ua.luxoft.market.Configuration;
import ua.luxoft.market.dao.DefaultData;
import ua.luxoft.market.dao.HibernateUtil;
import ua.luxoft.market.dao.model.Laptop;
import ua.luxoft.market.dao.model.LaptopType;
import ua.luxoft.market.dao.model.Producer;
import ua.luxoft.market.service.laptop.LaptopService;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = Configuration.class)
public class LaptopServiceTest {

    @Autowired
    private LaptopService laptopService;

    @BeforeClass
    public static void initData(){
        DefaultData.storeDefaultDataToH2();
    }

    @AfterClass
    public static void truncateData(){
        DefaultData.clearTables(Laptop.class.getSimpleName(), Producer.class.getSimpleName(), LaptopType.class.getSimpleName());
    }

    @Test
    public void testGetLaptop(){
        Laptop laptop = laptopService.getLaptop(1);
        Assert.assertNotNull(laptop);
        Assert.assertNotNull(laptop.getModel());
    }

    @Test
    public void testGetLaptops(){
        List<Laptop> laptops = laptopService.getLaptops();
        Assert.assertNotNull(laptops);
        Assert.assertTrue(laptops.size() > 0);
        Assert.assertNotNull(laptops.get(0));
        Assert.assertNotNull(laptops.get(0).getModel());
    }

    @Test
    public void testGetLaptopsByProducer(){
        List<Laptop> laptops = laptopService.getLaptopsByProducer(new Producer("Apple", 1));
        Assert.assertNotNull(laptops);
        Assert.assertEquals(2, laptops.size());
        Assert.assertNotNull(laptops.get(0));
        Assert.assertNotNull(laptops.get(0).getModel().startsWith("Apple MacBook"));
    }

    @Test
    public void addLaptop(){
        Laptop laptop = new Laptop("test", new Producer("Samsung", 3), new LaptopType("Budget", 5),"test", 1d);
        laptopService.createLaptop(laptop);
        laptop = laptopService.getLaptopByModel("test");
        Assert.assertNotNull(laptop);
        Assert.assertEquals(laptop.getModel(), "test");
    }

    @Test
    public void testDeleteLaptop(){
        List<Laptop> laptops = laptopService.getLaptopsByProducer(new Producer("Apple", 1));
        Assert.assertNotNull(laptops);
        Assert.assertTrue(laptops.size() > 0);
        Assert.assertNotNull(laptops.get(0));
        Assert.assertNotNull(laptops.get(0).getModel().startsWith("Apple MacBook"));
    }
}
