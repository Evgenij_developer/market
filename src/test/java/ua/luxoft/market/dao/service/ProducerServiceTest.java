package ua.luxoft.market.dao.service;

import junit.framework.Assert;
import org.hibernate.HibernateException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ua.luxoft.market.Configuration;
import ua.luxoft.market.dao.DefaultData;
import ua.luxoft.market.dao.model.Laptop;
import ua.luxoft.market.dao.model.LaptopType;
import ua.luxoft.market.dao.model.Producer;
import ua.luxoft.market.service.producer.ProducerService;

import java.util.List;

/**
 * Created by user on 11.09.15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = Configuration.class)
public class ProducerServiceTest {

    @Autowired
    private ProducerService producerService;

    @BeforeClass
    public static void initData(){
        DefaultData.storeDefaultDataToH2();
    }

    @AfterClass
    public static void truncateData(){
        DefaultData.clearTables(Laptop.class.getSimpleName(), Producer.class.getSimpleName(), LaptopType.class.getSimpleName());
    }

    @Test
    public void testGetProducers(){
        List<Producer> producers = producerService.getProducers();
        Assert.assertNotNull(producers);
    }

    @Test
    public void testGetProducer(){
        Producer producer = producerService.getProducer("Apple");
        Assert.assertNotNull(producer);
        Assert.assertEquals("Apple", producer.getName());
    }

    @Test
    public void testUpdateProducer(){
        Producer producer = producerService.getProducer("Acer");
        Assert.assertNotNull(producer);
        String newName = "Updated Acer";
        producer.setName(newName);
        producerService.updateProducer(producer);
        producer = producerService.getProducer(newName);
        Assert.assertNotNull(producer);
        Assert.assertEquals(newName, producer.getName());
    }

    @Test(expected = HibernateException.class)
    public void testDeleteProducer(){
        Producer producer = producerService.getProducer("Asus");
        Assert.assertNotNull(producer);
        producerService.deleteProducer(producer);
    }
}
