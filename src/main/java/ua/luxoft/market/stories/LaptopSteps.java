package ua.luxoft.market.stories;

import org.jbehave.core.annotations.*;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ua.luxoft.market.dao.DefaultData;
import ua.luxoft.market.dao.model.Laptop;
import ua.luxoft.market.dao.model.LaptopType;
import ua.luxoft.market.dao.model.Producer;
import ua.luxoft.market.service.laptop.LaptopService;
import java.util.List;

@Component
public class LaptopSteps {

    @Autowired
    private LaptopService service;
    private List<Laptop> laptops;
    private boolean applePresent;

    @BeforeScenario
    public void beforeScentario(){
        laptops = null;
        DefaultData.storeDefaultDataToH2();
    }

    @Given("list with laptops")
    public void getLaptops(){
        laptops = service.getLaptops();
    }

    @When("list isn't empty and has producer $producer")
    public void whenListOfLaptopsNotEmpty(@Named("producer") String producer) {
        Assert.assertEquals(!laptops.isEmpty(), true);
        for (Laptop laptop: laptops){
            if (laptop.getProducer().getName().equals(producer)) {
                applePresent = true;
                break;
            }
        }
        Assert.assertTrue(applePresent);
    }

    @Then("get model $model")
    public void thenGetModel(@Named("model") String model) {
        Assert.assertNotNull(laptops);
        Laptop laptop = null;
        for (Laptop lap : laptops) {
            if (lap.getModel().contains(model)) {
                laptop = lap;
                break;
            }
        }
        Assert.assertNotNull(laptop);
        Assert.assertTrue(laptop.getModel().contains(model));
    }

    @AfterScenario
    public void afterScentario(){
        DefaultData.clearTables(Laptop.class.getSimpleName(), LaptopType.class.getSimpleName(),
                Producer.class.getSimpleName());
    }

}
