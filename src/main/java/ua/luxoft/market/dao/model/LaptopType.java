package ua.luxoft.market.dao.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "laptop_type")
public class LaptopType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column
    private String type;

    public LaptopType() {}

    public LaptopType(String type) {
        this.type = type;
    }

    public LaptopType(String type, int id) {
        this.type = type;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
