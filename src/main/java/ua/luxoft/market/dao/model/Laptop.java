package ua.luxoft.market.dao.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "laptop")
public class Laptop implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(unique = true)
    private String model;
    @OneToOne(fetch = FetchType.LAZY, targetEntity = Producer.class)
    private Producer producer;
    @OneToOne(fetch = FetchType.LAZY, targetEntity = LaptopType.class)
    private LaptopType type;
    @Column
    private String description;
    @Column
    private Double price;

    public Laptop() {}

    public Laptop(String model, Producer producer, LaptopType type, String description, Double price) {
        this.model = model;
        this.producer = producer;
        this.type = type;
        this.description = description;
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    public LaptopType getType() {
        return type;
    }

    public void setType(LaptopType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
