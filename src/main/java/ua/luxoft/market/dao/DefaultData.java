package ua.luxoft.market.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import ua.luxoft.market.dao.model.Laptop;
import ua.luxoft.market.dao.model.LaptopType;
import ua.luxoft.market.dao.model.Producer;

/**
 * TODO; if the application goes to production, class need to move to the tests or in hibernate.hbm2ddl.import_files
 */
public class DefaultData {

    public static void storeDefaultDataToH2(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(new Producer("Apple"));
        session.save(new Producer("Asus"));
        session.save(new Producer("Samsung"));
        session.save(new Producer("Acer"));
        Producer appleProducer = (Producer) session.createQuery("from Producer where name='Apple'").list().get(0);
        Producer acerProducer = (Producer) session.createQuery("from Producer where name='Acer'").list().get(0);
        Producer asusProducer = (Producer) session.createQuery("from Producer where name='Asus'").list().get(0);
        Producer samsungProducer = (Producer) session.createQuery("from Producer where name='Samsung'").list().get(0);

        session.save(new LaptopType("Ultra book"));
        session.save(new LaptopType("Business"));
        session.save(new LaptopType("Game"));
        session.save(new LaptopType("Home"));
        session.save(new LaptopType("Budget"));
        LaptopType ultraBookType = (LaptopType) session.createQuery("from LaptopType where type='Ultra book'").list().get(0);
        LaptopType businessType = (LaptopType) session.createQuery("from LaptopType where type='Business'").list().get(0);
        LaptopType gameType = (LaptopType) session.createQuery("from LaptopType where type='Game'").list().get(0);
        LaptopType homeType = (LaptopType) session.createQuery("from LaptopType where type='Home'").list().get(0);
        LaptopType budgetType = (LaptopType) session.createQuery("from LaptopType where type='Budget'").list().get(0);

        session.save(new Laptop("Apple MacBook Air 13 (MJVE2UA/A) 2015", appleProducer, ultraBookType, "Intel Core i5, 1440x900px, Wi-Fi etc", 1000d));
        session.save(new Laptop("Apple MacBook Pro Retina 15.4 (MJLT2UA/A)", appleProducer, gameType, "Intel Core i7, 2880x1800px (Retina), Wi-Fi etc", 3000d));
        session.save(new Laptop("Acer Aspire ES1-311-P821 (NX.MRTEU.012)", acerProducer, homeType, "Intel Pentium N3540, (1366x768) WXGA HD, Wi-Fi etc", 450d));
        session.save(new Laptop("Acer Aspire E5-772G-31NN (NX.MV9EU.002)", acerProducer, ultraBookType, "Intel Core i3-4005U, 17.3 (1920x1080), 1Tb, Wi-Fi etc", 655d));
        session.save(new Laptop("Samsung R502", samsungProducer, budgetType, "Intel Pentium, 15.6, Wi-Fi etc", 200d));
        session.save(new Laptop("Asus Zenbook Pro UX501JW (UX501JW-CN115H)", asusProducer, gameType, "Intel Core i7-4720HQ, 15.6 (1900x1080) Full HD, Wi-Fi, etc", 2000d));
        session.save(new Laptop("Asus X553MA (X553MA-XX092D)", asusProducer, budgetType, " Intel Celeron N2830, 15.6 (1366x768) WXGA HD, 500Bg, Wi-Fi etc", 370d));
        session.save(new Laptop("Asus X554LJ (X554LJ-XO518D)", asusProducer, businessType, " Intel Core i3-5005U, 15.6 (1366x768) WXGA HD, Wi-Fi etc", 550d));
        session.save(new Laptop("Asus X751MD (X751MD-TY055D)", asusProducer, homeType, "Intel Pentium N3540, 17.3 (1600х900) HD+ LED, Wi-Fi etc", 500d));
        transaction.commit();
        session.close();
    }

    public static void clearTables(String... tables){
        Session sesssion = HibernateUtil.getSessionFactory().openSession();
        try {
            for (String table: tables){
                String hql = "delete from "+table;
                sesssion.createQuery(hql).executeUpdate();
            }
        } catch (HibernateException e){
            e.printStackTrace();
        }
        sesssion.close();

    }
}
