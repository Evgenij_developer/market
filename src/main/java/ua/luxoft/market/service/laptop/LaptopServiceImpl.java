package ua.luxoft.market.service.laptop;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import ua.luxoft.market.dao.HibernateUtil;
import ua.luxoft.market.dao.model.Laptop;
import ua.luxoft.market.dao.model.Producer;

import java.util.List;

/**
 * Created by user on 08.09.15.
 */
@Service
public class LaptopServiceImpl implements LaptopService {

    @Override
    public Laptop getLaptop(long id) {
        return (Laptop) HibernateUtil.getSession().createCriteria(Laptop.class)
                .add(Restrictions.eq("id", id)).uniqueResult();
    }

    @Override
    public Laptop getLaptopByModel(String model) {
        return (Laptop) HibernateUtil.getSession().createCriteria(Laptop.class)
                .add(Restrictions.eq("model", model)).uniqueResult();
    }

    @Override
    public List<Laptop> getLaptops() {
        return HibernateUtil.getSession().createCriteria(Laptop.class).list();
    }

    @Override
    public List<Laptop> getLaptopsByProducer(Producer producer) {
        return HibernateUtil.getSession().createCriteria(Laptop.class)
                .add(Restrictions.eq("producer", producer)).list();
    }

    @Override
    public void deleteLaptop(Laptop laptop) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        session.delete(laptop);
        transaction.commit();
        session.close();
    }

    @Override
    public void createLaptop(Laptop laptop) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        session.save(laptop);
        transaction.commit();
        session.close();
    }
}
