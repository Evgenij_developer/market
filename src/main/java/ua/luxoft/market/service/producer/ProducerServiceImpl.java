package ua.luxoft.market.service.producer;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;
import ua.luxoft.market.dao.HibernateUtil;
import ua.luxoft.market.dao.model.Producer;

import java.util.List;

@Service
public class ProducerServiceImpl implements ProducerService {

    @Override
    public void createProducer(String name) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.save(new Producer(name));
            transaction.commit();
        } catch (HibernateException e){
            e.printStackTrace();
            transaction.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public void updateProducer(Producer producer) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.update(producer);
            transaction.commit();
        } catch (HibernateException e){
            e.printStackTrace();
            transaction.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public void deleteProducer(Producer producer) {
        Session session = HibernateUtil.getSession();
        session.flush();
        session.clear();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(producer);
            transaction.commit();
        } catch (Exception e){
            e.printStackTrace();
            transaction.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public Producer getProducer(String name) {
        return (Producer) HibernateUtil.getSession().createQuery("from Producer where name=:prodname")
                .setParameter("prodname", name).uniqueResult();
    }

    @Override
    public List<Producer> getProducers() {
        return HibernateUtil.getSession().createCriteria(Producer.class).list();
    }
}
