package ua.luxoft.market.service.producer;

import org.springframework.stereotype.Service;
import ua.luxoft.market.dao.model.Producer;

import java.util.List;

@Service
public interface ProducerService {

    void createProducer(String name);
    void updateProducer(Producer producer);
    void deleteProducer(Producer producer);
    Producer getProducer(String name);
    List<Producer> getProducers();
}
