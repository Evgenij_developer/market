<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Shop best laptops at the lowest prices</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/styles.css">
</head>
<body>
    <h1>Welcome to our awesome shop!</h1>
    <table>
        <caption>Catalog of laptops</caption>
        <tr>
            <th>Producer</th>
            <th>Model</th>
            <th>Price</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
        <c:forEach items="${laptops}" var="laptop">
        <tr>
             <td>${laptop.producer.name}</td>
             <td>${laptop.model}</td>
             <td>${laptop.price}$</td>
             <td>${laptop.type.type}</td>
             <td>${laptop.description}</td>
        </tr>
        </c:forEach>
    </table>
    <div class="nextpage">If you want to manage producers <a href="${pageContext.request.contextPath}/producers">click here</a></div>
</body>
</html>
